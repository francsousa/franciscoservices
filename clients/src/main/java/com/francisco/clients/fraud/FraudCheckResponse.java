package com.francisco.clients.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
