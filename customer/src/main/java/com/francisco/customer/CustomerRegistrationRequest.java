package com.francisco.customer;

public record CustomerRegistrationRequest(String firstName, String lastName, String email) {
}
